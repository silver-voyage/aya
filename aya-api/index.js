const app = require('express')()
const bodyParser = require('body-parser')
const cors = require('cors')
const mongoose = require('mongoose')
const Ride = require('./models/ride')

// setup POST requests
app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

// mongoose setup
mongoose.connect('mongodb+srv://admin:JhhDM3eVpLyZcLLC@cluster0.yrmys.mongodb.net/myFirstDatabase?retryWrites=true&w=majority')

// https requests
// welcome message
app.get('/', (req, res) => {
  res.json({ message: 'Hooray! Welcome to aya-api!' })
})

// all rides
app.get('/rides', async (req, res) => {
  const rides = await Ride.find({})
  res.json({ rides })
})

// get ride by id
app.get('/ride/:id', async (req, res) => {
  Ride.findById(req.params.id, (err, ride) => {
    res.json({ ride })
  })
})

// create ride
app.post('/ride', async (req, res) => {
  const { destination, startPoint, name } = req.body

  const delivery = new Ride({
    name,
    destination: {
      latitude: destination.latitude,
      longitude: destination.longitude,
    },
    startPoint,
    locations: startPoint,
  })

  delivery.save((err, payload) => {
    res.json({ ride: payload })
  })
})

// update ride
app.put('/ride/:id', async (req, res) => {
  const id = { _id: req.params.id }

  const data = {
    $push: {
      locations: req.body.location,
    },
  }

  Ride.update(id, data, (err, data) => {
    Ride.findById(req.params.id, (err, ride) => {
      res.json({ ride })
    })
  })
})

// complete ride
app.post('/ride/:id/complete', async (req, res) => {
  const id = { _id: req.params.id }
  const data = { completed: true }

  Ride.update(id, data, (err, data) => {
    Ride.findById(req.params.id, (err, ride) => {
      res.json({ ride })
    })
  })
})

// clear all rides
app.get('/rides/clear', async (req, res) => {
  await Ride.remove({})
  res.json({ message: 'Databased cleared' })
})

// start the app
app.listen(process.env.PORT || 3001)
console.log('[aya-api] Running on port 3001. http://localhost:3001')