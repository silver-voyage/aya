const mongoose = require('mongoose')

const ride = new mongoose.Schema({
  name: String,
  destination: {
    latitude: String,
    longitude: String,
  },
  startPoint: {
    latitude: String,
    longitude: String,
  },
  locations: [{
    latitude: String,
    longitude: String,
  }],
  completed: {
    type: Boolean,
    default: false,
  },
}, { timestamps: true })

module.exports = mongoose.model('Ride', ride)